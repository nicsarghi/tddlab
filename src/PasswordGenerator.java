import java.util.Random;
public class PasswordGenerator {
    private int passwordLength;
    
    public PasswordGenerator(int passLength){
        this.passwordLength = passLength;
    }

    //returns password equal to length
    public String generateWeakPassword(){
        String weak = "abcdefghijklmnopqrstuvwxyz";
        Random randy = new Random(weak.length());
        StringBuilder strBuilder = new StringBuilder();
        
        for(int i = 0; i < this.passwordLength; i++){
            strBuilder.append(weak.charAt(randy.nextInt()));
        }
        return strBuilder.toString();
    }
    //returns password equal to 2x length
    public String generateStrongPassword(){
        String weak = "abcdefghijklmnopqrstuvwxyz";
        Random randy = new Random(weak.length());
        StringBuilder strBuilder = new StringBuilder();
        
        for(int i = 0; i < (this.passwordLength * 2); i++){
            strBuilder.append(weak.charAt(randy.nextInt()));
        }
        return strBuilder.toString();
    }
}
