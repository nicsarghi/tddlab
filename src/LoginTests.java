import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class LoginTests {
    @Test
    public void validateTest(){
        User[] users = new User[2];
        users[0] = new User("Slorp", "password123", UserType.REGULAR);
        users[1] = new User("Hehe", "hehe123",  UserType.ADMIN);
        Login login = new Login(users);
        assertEquals(true, login.validate("Slorp", "password123", false));
        assertEquals(false, login.validate("Slorp", "password123", true));
        //assertEquals(expected, actual);


    }
}
